import argparse
import gitlab
import os

import project_management as pm


def command_line_options():
    parser = argparse.ArgumentParser(description="""Tuco: adding CI spices to
                                                  your package. Configure gitlab ci config path and creates merge
                                                  request with needed configuration file with automerge if pipeline
                                                  success.""")
    parser.add_argument('-m', '--master-branch', type=str, dest='master_branch_name',
                        default=pm.default_master_branch_name,
                        help='Name of the master branch (default: {}).'.format(pm.default_master_branch_name))
    parser.add_argument('-i', '--initialization-branch', type=str, dest='init_branch_name',
                        default=pm.default_init_branch_name,
                        help='Name of the initialization working branch (default: {}).'.format(
                            pm.default_init_branch_name))
    parser.add_argument('-c', '--ci-config-path', type=str, dest='ci_config_path', default=pm.default_path_to_ci_yml,
                        help='CI configuration file path (default: {}).'.format(pm.default_path_to_ci_yml))
    parser.add_argument('-r', '--release', type=str, default=pm.default_release,
                        help='CI configuration file path (default: {}).'.format(pm.default_release))
    parser.add_argument('-n', '--non-free-deps', action='store_true',
                        help='Active this option if non free dependencies needed.')
    parser.add_argument('url', type=str, help='URL of the project to be initialize.')

    return parser.parse_args()


if __name__ == '__main__':
    args = command_line_options()

    if args.url:
        private_token = os.environ['GITLAB_API_TOKEN']
        cli = gitlab.Gitlab('https://salsa.debian.org', private_token=private_token)
        cli.auth()
        project = pm.get_project_by_url(cli, args.url)

        last_commit = pm.initialize_project(cli, project,
                                            master_branch_name=args.master_branch_name,
                                            init_branch_name=args.init_branch_name,
                                            ci_config_path=args.ci_config_path,
                                            release=args.release,
                                            non_free_deps=args.non_free_deps,
                                            )
        pm.merge_initialized_branch(project, last_commit,
                                    init_branch_name=args.init_branch_name,
                                    master_branch_name=args.master_branch_name,
                                    )
